# README

### Summary

* Front end files for the supply-side server (supply.team24.softwareengineeringii.com).

### Navigation

* `index.css`
    * Styling for HTML pages.
* `register.html`
    * Page where fleet managers can create an account.
* `login.html`
    * Page where fleet managers can log in.
* `vehicleRegistration.html`
    * Page where fleet managers can register a vehicle.
* `geocoding.html`
    * Mapping test page - put in an address and map moves to that address.
* `moving_icon.html`
    * Mapping test page.
* `more_route_testing.html`
    * Mapping route test page.

### Team members

* TM1 - Sydney Mack
* TM2 - Mariah Sager
* TM3 - Keldon Boswell
* TM4 - Colby Hayes
* TM5 - Karla Salto
